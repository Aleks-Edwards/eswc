import fasttext
import re
import sqlite3



conn = sqlite3.connect('')
cur = conn.cursor()


def createModelAndPredict():

    model = fasttext.train_supervised(input="crime.train", minn=2, maxn=8, lr=0.2, epoch=100, wordNgrams=2, bucket=20000,dim=300, thread=1, loss='ova')


    lines = []

    with open('full_docs.valid', 'r') as f:

        line = f.readline()
        lines.append(line)

        while line:
            line = f.readline()
            if line != '':
                lines.append(line)
                #print(line)


    sentences_with_labels = {}
    for sent_label in lines:
        sent = re.sub('__label__\d+', '', sent_label)
        sent = sent.replace('\n', '')
        actuallabels = re.findall('__label__\d+', sent_label, re.S)
        cleanedlabels = []
        for l in actuallabels:
            l = l.replace('__label__', '').strip()
            cleanedlabels.append(l)

        #predictions_per_sent = model.predict(sent, k=4)
        predictions_per_sent = model.predict(sent, k=5)
        predicted_labels = []
        for i in range(0, len(predictions_per_sent[0])):
            if float(predictions_per_sent[1][i]) >= 0.15:
            #if float(predictions_per_sent[1][i]) >= 0.0:
                cleaned_pred = predictions_per_sent[0][i].replace('__label__', '').strip()
                predicted_labels.append(cleaned_pred)



        if not predicted_labels:

            new_pred = model.predict(sent, k=5)

            probs = list(new_pred[1])

            indexel = list(new_pred[1]).index(max(new_pred[1]))
            labels = list(new_pred[0])


            cl_pred = [new_pred[0][0].replace('__label__', '').strip()]
            cl_pred = labels[indexel].replace('__label__', '').strip()
            cl_pred1 = labels[1].replace('__label__', '').strip()
            cl_pred2 = labels[2].replace('__label__', '').strip()
            predicted_labels.append(cl_pred)

            sentences_with_labels[sent] = [cleanedlabels,predicted_labels]

        else:

            sentences_with_labels[sent.strip()] = [cleanedlabels, predicted_labels]
    return sentences_with_labels

def getSentIDS(sentences_with_labels):

    for s in sentences_with_labels.keys():
        #print(s)
        #print(sentences_with_labels[s][1])

        fasttext_label = str(sentences_with_labels[s][1]).replace("'","")
        print(fasttext_label)
        print("-----------------")
        sql_up = "UPDATE doc_sent_full SET label = '" + fasttext_label + "' where cl_sent = '" + str(s).strip() + "' and label is null;"
        print(sql_up)
        cur.execute(sql_up)
        conn.commit()

def evaluateModel(sentences_with_labels):
    tp = {}
    fp = {}
    fn = {}
    count_1_tp = 0
    count_2_tp = 0
    count_3_tp = 0
    count_4_tp = 0
    count_5_tp = 0

    count_1_fp = 0
    count_2_fp = 0
    count_3_fp = 0
    count_4_fp = 0
    count_5_fp = 0

    count_1_fn = 0
    count_2_fn = 0
    count_3_fn = 0
    count_4_fn = 0
    count_5_fn = 0

    for s in sentences_with_labels.keys():
        actuallabels = sentences_with_labels[s][0]
        predictedlabels = sentences_with_labels[s][1]


        if '1' in actuallabels and '1' in predictedlabels:
            count_1_tp = count_1_tp+1

        if '1' in actuallabels and '1' not in predictedlabels:
            count_1_fn = count_1_fn + 1

        if '1' not in actuallabels and '1' in predictedlabels:
            count_1_fp = count_1_fp + 1

        if '2' in actuallabels and '2' in predictedlabels:
            count_2_tp = count_2_tp+1

        if '2' in actuallabels and '2' not in predictedlabels:
            count_2_fn = count_2_fn + 1

        if '2' not in actuallabels and '2' in predictedlabels:
            count_2_fp = count_2_fp + 1

        if '3' in actuallabels and '3' in predictedlabels:
            count_3_tp = count_3_tp+1

        if '3' in actuallabels and '3' not in predictedlabels:
            count_3_fn = count_3_fn + 1

        if '3' not in actuallabels and '3' in predictedlabels:
            count_3_fp = count_3_fp + 1

        if '4' in actuallabels and '4' in predictedlabels:
            count_4_tp = count_4_tp+1

        if '4' in actuallabels and '4' not in predictedlabels:
            count_4_fn = count_4_fn + 1

        if '4' not in actuallabels and '4' in predictedlabels:
            count_4_fp = count_4_fp + 1

        if '5' in actuallabels and '5' in predictedlabels:
            count_5_tp = count_5_tp+1

        if '5' in actuallabels and '5' not in predictedlabels:
            count_5_fn = count_5_fn + 1

        if '5' not in actuallabels and '5' in predictedlabels:
            count_5_fp = count_5_fp + 1


    tp['1'] = count_1_tp
    tp['2'] = count_2_tp
    tp['3'] = count_3_tp
    tp['4'] = count_4_tp
    tp['5'] = count_5_tp

    fn['1'] = count_1_fn
    fn['2'] = count_2_fn
    fn['3'] = count_3_fn
    fn['4'] = count_4_fn
    fn['5'] = count_5_fn

    fp['1'] = count_1_fp
    fp['2'] = count_2_fp
    fp['3'] = count_3_fp
    fp['4'] = count_4_fp
    fp['5'] = count_5_fp

    av_precision = []
    av_recall = []

    all_trues = []
    all_falsepos = []
    all_falseneg = []

    all_trues_falsepos = []
    all_trues_falseneg = []

    for k in tp.keys():
        all_trues.append(tp[k])
        all_falsepos.append(fp[k])
        all_falseneg.append(fn[k])

        all_trues_falsepos.append(tp[k])
        all_trues_falsepos.append(fp[k])

        all_trues_falseneg.append(tp[k])
        all_trues_falseneg.append(fn[k])

        precision = float(tp[k]) / (tp[k] + fp[k])
        av_precision.append(precision)
        recall = float(tp[k]) / (tp[k] + fn[k])
        av_recall.append(recall)
        f1 = 2 * ((precision * recall) / (precision + recall))
        print(k, ':::', "p: ", precision, "r: ", recall, "f1: ", f1)

    avP = Average(av_precision)
    avR = Average(av_recall)
    print(avP)
    print(avR)

    print("All trues: ", all_trues)
    print("All false pos: ", all_falsepos)
    print("All false neg: ", all_falseneg)
    print("All trues and false pos: ", all_trues_falsepos)
    print("All trues and false neg: ", all_trues_falseneg)
    micro_precision = float(sum(all_trues)) / sum(all_trues_falsepos)
    micro_recall = float(sum(all_trues)) / sum(all_trues_falseneg)
    micro_f1 = 2 * ((micro_precision * micro_recall) / (micro_precision + micro_recall))

    macro_precision = avP
    macro_recall = avR
    macro_f1 = 2 * ((macro_precision * macro_recall) / (macro_precision + macro_recall))

    print("micro_precision: ", micro_precision)
    print("micro_recall: ", micro_recall)
    print("micro_f1: ", micro_f1)
    print("------------------------------")
    print("macro_precision: ", macro_precision)
    print("macro_recall: ", macro_recall)
    print("macro_f1: ", macro_f1)

def Average(lst):
    return sum(lst) / len(lst)

def main():

    sentences_with_labels = createModelAndPredict()
    #getSentIDS(sentences_with_labels)
    #evaluateModel(sentences_with_labels)

main()