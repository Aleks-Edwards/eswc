import sqlite3
import numpy as np
from fse.models import Average
from fse.models import SIF
from fse.models import uSIF
from fse import IndexedList
from gensim.models import KeyedVectors
from sklearn.feature_extraction.text import TfidfVectorizer

conn = sqlite3.connect('')
cur = conn.cursor()

def getIDSNotInclude(tokens_table):
    cleaned_ids = []
    word_sent_ids = {}
    toremove = []

    #sql = "SELECT token_id from '"+tokens_table+"' where stop_word = 'no' and v_exist = 'yes' and lemma not in ('ellie','m','rb','thomson','jane','p','leva','jk','ab','sharon','hestia','v','wx','marie','rose','jade','fl','vs','dont','j','w');"
    sql = "SELECT token_id from '"+tokens_table+"' where stop_word = 'no' and avgvec  is not 'None' and lemma not in ('ellie','m','rb','thomson','jane','p','leva','jk','ab','sharon','hestia','v','wx','marie','rose','jade','fl','vs','dont','j','w');"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0].strip()
        token_id_list = token_id.split(".")
        sent_id = token_id_list[0] + "." + token_id_list[1]
        try:
            word_sent_ids[sent_id].append(token_id)
        except KeyError:
            word_sent_ids[sent_id] = [token_id]


    for sid in word_sent_ids.keys():
        tokens_ids = word_sent_ids[sid]
        for tid in tokens_ids:
            for tidmatching in tokens_ids:
                if tid != tidmatching:
                    tid_start = int(tid.split(".")[2])
                    tid_end = int(tid.split(".")[3])
                    tidmatching_start = int(tidmatching.split(".")[2])
                    tidmatching_end = int(tidmatching.split(".")[3])
                    if tidmatching_start >= tid_start and tidmatching_end <= tid_end:
                        toremove.append(tidmatching)


    for key in word_sent_ids.keys():
        for item in word_sent_ids[key]:
            if item not in toremove:
                cleaned_ids.append(item.strip())

    cleaned_ids = str(cleaned_ids).replace("[", "(").replace("]", ")")

    return cleaned_ids

def buildsentFromTokens(tokens_table,sentemb_table):
    cleaned_ids = getIDSNotInclude(tokens_table)
    word_sent = {}
    sql = "SELECT token_id, lemma from '" + tokens_table + "' where token_id in " + cleaned_ids + ";"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0].strip().split(".")
        sent_id = token_id[0] + "." + token_id[1]
        #lemma = row[1].strip().replace("_", "").strip()
        lemma = row[1].strip().replace("_", "-").strip()

        try:
            word_sent[sent_id].append(lemma)
        except KeyError:
            word_sent[sent_id] = [lemma]

    for sent_id in word_sent.keys():

        tokens = []
        for token in word_sent[sent_id]:
            token = token.strip()
            tokens.append(token)

        sent = " ".join(t for t in tokens)
        print(sent_id)
        print(sent)
        print("----------------------------")
        sql_ins = "INSERT INTO '"+sentemb_table+"'(sent_id, sent) VALUES('"+sent_id+"', '"+sent+"');"
        cur.execute(sql_ins)
        conn.commit()

def cleanSent(sentemb_table):
    original_sent = {}
    sent_emb = {}
    original_ids = []
    sent_emb_ids = []


    #print("The number of sentences between the sent embeddings and the sent part of the original classifier missmatch, thus we delete the sent that has not been taken into account during the original classifier")
    sql_original = "SELECT sent_id, sentence FROM intitial_classifier_train;"
    cur.execute(sql_original)
    res = cur.fetchall()
    for row in res:
        sent_id = row[0]
        sent = row[1]
        original_sent[sent_id] = [sent]
        original_ids.append(sent_id)


    sql_semb = "SELECT sent_id, sent FROM '"+sentemb_table+"';"
    cur.execute(sql_semb)
    res_emb = cur.fetchall()
    for row in res_emb:
        sid = row[0]
        sent = row[1]
        sent_emb[sid] = [sent]
        sent_emb_ids.append(sid)

    for id in sent_emb_ids:
        if id not in original_ids:
            sql_del = "DELETE FROM '"+sentemb_table+"' where sent_id = '"+id.strip()+"';"
            cur.execute(sql_del)
            conn.commit()


def insertSentLabels(sentemb_table):
    original_labels = {}
    sql_labels = "SELECT sent_id, label FROM intitial_classifier_train;"
    cur.execute(sql_labels)
    res_labels = cur.fetchall()
    for row in res_labels:
        sid = row[0].strip()
        label = row[1].strip()
        sql = "UPDATE '"+sentemb_table+"' SET label = '"+label+"' WHERE sent_id = '"+sid+"';"
        cur.execute(sql)
        conn.commit()



    #print("INSERT CLASS LABELS IN embeddings table;")


def manualAvgSentEmb(tokens_table,sentemb_table):
    token_ids = {}
    cleaned_ids = getIDSNotInclude(tokens_table)
    #sql = "SELECT token_id, vector FROM '" + tokens_table + "' WHERE token_id in "+cleaned_ids+" and vector is not null;"
    #sql = "SELECT token_id, learned_vec FROM '" + tokens_table + "' WHERE token_id in " + cleaned_ids + " and learned_vec is not null;"
    #sql = "SELECT token_id, learned_t FROM '" + tokens_table + "' WHERE token_id in " + cleaned_ids + " and learned_t is not null;"
    sql = "SELECT token_id, avgvec FROM '" + tokens_table + "' WHERE token_id in " + cleaned_ids + " and avgvec is not null and avgvec != 'None';"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0]
        tid_list = row[0].split(".")
        sent_id = tid_list[0] + "." + tid_list[1]
        vec = row[1].replace("[", "").replace("]", "")
        vec_list = []
        if "," in vec:
        #for val in vec.split():
            for val in vec.split(","):
                val = val.strip()
                val = float(val)
                vec_list.append(val)
        else:
            for val in vec.split():
                val = val.strip()
                val = float(val)
                vec_list.append(val)


        try:
            token_ids[sent_id].append(vec_list)
        except KeyError:
            token_ids[sent_id] = [vec_list]




    for sent_id in token_ids.keys():

        sent2vec = np.mean(token_ids[sent_id], axis=0)
        #sql = "UPDATE '" +sentemb_table+ "' SET specialised_vec = '" + str(sent2vec) + "' WHERE sent_id = '" + sent_id.strip() + "';"
        #sql = "UPDATE '" + sentemb_table + "' SET specialised_vec_t = '" + str(sent2vec) + "' WHERE sent_id = '" + sent_id.strip() + "';"
        sql = "UPDATE '" + sentemb_table + "' SET avg_spvec_avg = '" + str(sent2vec) + "' WHERE sent_id = '" + sent_id.strip() + "';"
        cur.execute(sql)
        conn.commit()

    #print("IMPLEMENT SIMPLE manual avg sentence build based on word embeddings")

def buildSentForTFIDF(tokens_table):
    cleaned_ids = getIDSNotInclude(tokens_table)

    word_sent = {}
    sql = "SELECT token_id, lemma from '"+tokens_table+"' where token_id in "+cleaned_ids+";"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0].strip().split(".")
        sent_id = token_id[0] + "." + token_id[1]
        lemma = row[1].strip()

        try:
            word_sent[sent_id].append(lemma)
        except KeyError:
            word_sent[sent_id] = [lemma]

    allsentences = []
    for sent_id in word_sent.keys():

        tokens = []
        for token in word_sent[sent_id]:
            token = token.strip()
            tokens.append(token)

        sent = " ".join(t for t in tokens)

        allsentences.append([sent_id,sent])

    return allsentences,word_sent

def calcNewTFIDF(tokens_table):
    allsentences,word_sent = buildSentForTFIDF(tokens_table)

    sids = []
    corpus = []
    for item in allsentences:
        sids.append(item[0].strip())
        corpus.append(item[1])

    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(corpus)
    wordslist = vectorizer.get_feature_names()

    tfidfscores = X.toarray()
    scoresbyids = {}
    for i in range(0,len(sids)):

        cleaned_scores = []
        for j in range(0,len(tfidfscores[i])):
            score = float(tfidfscores[i][j])
            if wordslist[j] in word_sent[sids[i]]:
                cleaned_scores.append([wordslist[j],score])

        scoresbyids[sids[i]] = cleaned_scores

    for sid in scoresbyids.keys():
        for t in range(0,len(scoresbyids[sid])):

            print(sid,scoresbyids[sid][t][0],scoresbyids[sid][t][1])
            print("-------------------------")
            sql = "UPDATE '"+tokens_table+"' SET tfidf_score = '"+str(scoresbyids[sid][t][1])+"' WHERE token_id like '"+sid.strip()+".%' and lemma = '"+scoresbyids[sid][t][0].strip()+"';"
            cur.execute(sql)
            conn.commit()

def updateWeightedVecWordEmb(tokens_table):
    weightedwordvectors = {}
    cleaned_ids = getIDSNotInclude(tokens_table)
    sql = "SELECT token_id, vector, tfidf_score FROM '"+tokens_table+"' WHERE token_id in "+cleaned_ids+" and tfidf_score is not null;"

    cur.execute(sql)
    res = cur.fetchall()
    for row in res:

        token_id = row[0]
        vec = row[1].replace("[","").replace("]","").split(",")
        tfidf = float(row[2])

        weighted_vec = []
        for val in vec:
            val = float(val.strip())
            weighted_val = val*tfidf
            if weighted_val == -0.0:
                weighted_val = 0.0
            weighted_vec.append(weighted_val)
        weightedwordvectors[token_id] = weighted_vec

    for tid in weightedwordvectors.keys():

        sql_up = "UPDATE '"+tokens_table+"' SET weighted_vec = '"+str(weightedwordvectors[tid])+"' WHERE token_id = '"+tid.strip()+"';"
        cur.execute(sql_up)
        conn.commit()


def tfidfweightedAvgWordEmb(tokens_table):
    wordsimportance = []
    wordvectors = {}
    sql_imp = "SELECT token, tf_idf_rank FROM tokens_newapp_importance;"
    cur.execute(sql_imp)
    res_imp = cur.fetchall()
    for row in res_imp:
        token = row[0]
        tf_idf_rank = row[1]
        tf_idf_rank = float(tf_idf_rank)
        wordsimportance.append([token,tf_idf_rank])

    wordsvectors = []

    sql_vec = "SELECT lemma, vector FROM '"+tokens_table+"' WHERE v_exist = 'yes' and stop_word = 'no';"
    cur.execute(sql_vec)
    res_vec = cur.fetchall()

    for row in res_vec:
        lemma = row[0]
        vector = row[1].replace("[","").replace("]","")
        vector_l = vector.split(",")

        float_vals = []
        for val in vector_l:
            val = val.strip()
            val = float(val)
            float_vals.append(val)

        if [lemma,float_vals] not in wordsvectors:
            wordsvectors.append([lemma,float_vals])


    weightedvec = []
    for i in range(0,len(wordsimportance)):
        for j in range(0,len(wordsvectors)):
            if wordsimportance[i][0] == wordsvectors[j][0]:
                float_vector = wordsvectors[j][1]
                tf_idf = wordsimportance[i][1]
                weighted_vec = []
                for val in float_vector:
                    weighted_val = val*tf_idf

                    if weighted_val == -0.0:
                        weighted_val = 0.0

                    weighted_vec.append(weighted_val)

                if [wordsvectors[j][0],weighted_vec] not in weightedvec:
                    weightedvec.append([wordsvectors[j][0],weighted_vec])

    for item in weightedvec:
        sql = "UPDATE '"+tokens_table+"' SET weighted_vec = '"+str(item[1])+"' WHERE lemma = '"+item[0].strip()+"';"
        cur.execute(sql)
        conn.commit()

def manualTFIDFSentEmb(tokens_table,sentemb_table):
    token_ids = {}
    cleaned_ids = getIDSNotInclude(tokens_table)
    #tfidfweightedAvgWordEmb(tokens_table)
    sql = "SELECT token_id, weighted_vec FROM "+tokens_table+" WHERE token_id in"+cleaned_ids+" and stop_word = 'no' and weighted_vec != '0.0';"

    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0]
        tid_list = row[0].split(".")
        sent_id = tid_list[0] + "." + tid_list[1]
        vec = row[1].replace("[", "").replace("]", "")

        vec_list = []

        for val in vec.split(","):
            val = val.strip()

            val = float(val)
            vec_list.append(val)

        try:
            token_ids[sent_id].append(vec_list)
        except KeyError:
            token_ids[sent_id] = [vec_list]

    for sent_id in token_ids.keys():
        sent2vec = np.mean(token_ids[sent_id], axis=0)
        sql = "UPDATE '"+sentemb_table+"' SET tfidf = '"+str(sent2vec)+"' WHERE sent_id = '"+sent_id.strip()+"';"
        cur.execute(sql)
        conn.commit()



def mlAvgSentEmb(sentemb_table):
    sentences = []

    sql = "SELECT sent_id, sent FROM '" +sentemb_table+ "';"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent = row[1].split(" ")
        sentences.append(sent)

    ft = KeyedVectors.load_word2vec_format('model_skipgram.vec')
    model = Average(ft)

    model.train(IndexedList(sentences))
    for i in range(0, len(sentences)):
        tokens = []
        for t in sentences[i]:
            tokens.append(t.strip())

        sent_whole = " ".join(t for t in tokens)
        #print(sent_whole)

        #sql_up = "UPDATE '"+sentemb_table+"' SET mlavg = '"+str(model.sv[i])+"' WHERE sent = '"+sent_whole+"';"
        sql_up = "UPDATE '" + sentemb_table + "' SET specialised_vec = '" + str(model.sv[i]) + "' WHERE sent = '" + sent_whole + "';"
        cur.execute(sql_up)
        conn.commit()

    #print("IMPLEMENT avg machine learning sent embedding")

def sifSentEmb(sentemb_table):
    sentences = []

    sql = "SELECT sent_id, sent FROM '" + sentemb_table + "';"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent = row[1].split(" ")
        sentences.append(sent)

    ft = KeyedVectors.load_word2vec_format('model_skipgram.vec')
    model = SIF(ft, components=10, lang_freq="en")

    model.train(IndexedList(sentences))
    for i in range(0, len(sentences)):
        tokens = []
        for t in sentences[i]:
            tokens.append(t.strip())

        sent_whole = " ".join(t for t in tokens)
        print("sif: ",sent_whole)
        #print(model.sv[i])
        sql_up = "UPDATE '" + sentemb_table + "' SET spvecsif = '" + str(model.sv[i]) + "' WHERE sent = '" + sent_whole + "';"
        cur.execute(sql_up)
        conn.commit()



def usifSentEmb(sentemb_table):
    sentences = []

    sql = "SELECT sent_id, sent FROM '" + sentemb_table + "';"

    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent = row[1].split(" ")
        sentences.append(sent)

    ft = KeyedVectors.load_word2vec_format('avg.vec')
    model = uSIF(ft, components=10, lang_freq="en")

    model.train(IndexedList(sentences))
    for i in range(0, len(sentences)):
        tokens = []
        for t in sentences[i]:
            tokens.append(t.strip())

        sent_whole = " ".join(t for t in tokens)
        #sql_up = "UPDATE '" + sentemb_table + "' SET usif = '" + str(model.sv[i]) + "' WHERE sent = '" + sent_whole + "';"
        #sql_up = "UPDATE '" + sentemb_table + "' SET spvecusif = '" + str(model.sv[i]) + "' WHERE sent = '" + sent_whole + "';"
        sql_up = "UPDATE '" + sentemb_table + "' SET avg_spvec_usif = '" + str(model.sv[i]) + "' WHERE sent = '" + sent_whole + "';"
        print(sql_up)
        cur.execute(sql_up)
        conn.commit()
    #print("IMPLEMENT uSIF machine learning sent embedding")

def minBuildSent(tokens_table,sentemb_table):
    token_ids = {}
    cleaned_ids = getIDSNotInclude(tokens_table)

    sql = "SELECT token_id, vector FROM " + tokens_table + " WHERE token_id in" + cleaned_ids + " and stop_word = 'no';"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0]
        tid_list = row[0].split(".")
        sent_id = tid_list[0] + "." + tid_list[1]
        vec = row[1].replace("[", "").replace("]", "")

        vec_list = []

        for val in vec.split(","):
            val = val.strip()

            val = float(val)
            vec_list.append(val)

        try:
            token_ids[sent_id].append(vec_list)
        except KeyError:
            token_ids[sent_id] = [vec_list]


    for sent_id in token_ids.keys():
        sent2vec = np.min(token_ids[sent_id],axis=0)
        sql = "UPDATE '"+sentemb_table+"' SET minim = '"+str(sent2vec)+"' WHERE sent_id = '"+sent_id.strip()+ "';"
        cur.execute(sql)
        conn.commit()

def maxBuildSent(tokens_table, sentemb_table):
    token_ids = {}
    cleaned_ids = getIDSNotInclude(tokens_table)

    sql = "SELECT token_id, vector FROM "+tokens_table+" WHERE token_id in"+cleaned_ids+" and stop_word = 'no';"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        token_id = row[0]
        tid_list = row[0].split(".")
        sent_id = tid_list[0] + "." + tid_list[1]
        vec = row[1].replace("[", "").replace("]", "")

        vec_list = []

        for val in vec.split(","):
            val = val.strip()

            val = float(val)
            vec_list.append(val)

        try:
            token_ids[sent_id].append(vec_list)
        except KeyError:
            token_ids[sent_id] = [vec_list]

    for sent_id in token_ids.keys():
        sent2vec = np.max(token_ids[sent_id], axis=0)
        sql = "UPDATE '"+sentemb_table+"' SET maxim = '"+str(sent2vec)+"' WHERE sent_id = '"+sent_id.strip()+"';"
        cur.execute(sql)
        conn.commit()


def combineMMMBuildSent(sentemb_table):
    sql = "Select sent_id, manavg, minim, maxim from "+sentemb_table+";"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        sent_id = row[0]
        manavg = row[1].replace("[","").replace("]","").split()
        minim = row[2].replace("[","").replace("]","").split()
        maxim = row[3].replace("[","").replace("]","").split()

        manavg_l = []
        minim_l = []
        maxim_l = []

        for i in range(0,len(manavg)):
            avg = manavg[i].strip()
            avg = float(avg)
            manavg_l.append(avg)
            min =  minim[i].strip()
            min = float(min)
            minim_l.append(min)
            max = maxim[i].strip()
            max = float(max)
            maxim_l.append(max)

        concat_minmaxmean = manavg_l+minim_l+maxim_l
        sql_up = "UPDATE '"+sentemb_table+"' SET minmax= '"+str(concat_minmaxmean)+"' WHERE sent_id = '"+sent_id.strip()+"';"
        cur.execute(sql_up)
        conn.commit()


def main():
    #tokens_table = 'tokens_training_google'
    tokens_table = 'tokens_training_fastext'
    sentemb_table = 'sentvectors_embeddings_fastext'
    #sentemb_table = 'cleaned_lemmatised'
    #sentemb_table = 'specialised_voc_terms'
    #buildsentFromTokens(tokens_table,sentemb_table)
    #cleanSent(sentemb_table)
    #insertSentLabels(sentemb_table)
    #manualAvgSentEmb(tokens_table,sentemb_table)
    #manualTFIDFSentEmb(tokens_table, sentemb_table)
    #mlAvgSentEmb(sentemb_table)
    #sifSentEmb(sentemb_table)
    usifSentEmb(sentemb_table)
    #minBuildSent(tokens_table, sentemb_table)
    #maxBuildSent(tokens_table, sentemb_table)
    #combineMMMBuildSent(sentemb_table)
    #calcNewTFIDF(tokens_table)

    #updateWeightedVecWordEmb(tokens_table)
    #manualTFIDFSentEmb(tokens_table, sentemb_table)

main()