import re
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
from nltk.stem import WordNetLemmatizer
import sqlite3
import spacy
spacy.load("en_core_web_sm")
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import preprocessing
from sklearn.naive_bayes import ComplementNB
import pickle
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import classification_report
import warnings
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.model_selection import cross_validate
from sklearn.metrics import recall_score




conn = sqlite3.connect('')
cur = conn.cursor()


def readDB():
    l = []
    l_stats = []
    l_nodup = []
    l_nodup_stats = []

    sql = "select t.sent_id, t.sent, t.theme_id from themes_sentences_grouped t, sentvectors_embeddings_google s where t.sent_id = s.sent_id;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = row[0]
        sent = row[1]
        theme = row[2]

        if theme.startswith('1.'):
            theme_overall = 1
            theme_name = "Contact with Agencies"
            l.append([sent_id, sent, str(theme_overall)])
            l_stats.append([sent_id, sent, str(theme_name)])

        if theme.startswith('2.'):
            theme_overall = 2
            theme_name = "Indicative Behaviour"
            l.append([sent_id, sent, str(theme_overall)])
            l_stats.append([sent_id, sent, str(theme_name)])

        if theme.startswith('3.'):
            theme_overall = 3
            theme_name = "Indicative Circumstances"
            l.append([sent_id, sent, str(theme_overall)])
            l_stats.append([sent_id, sent, str(theme_name)])

        if theme.startswith('4.'):
            theme_overall = 4
            theme_name = "Mental Health Issues"
            l.append([sent_id, sent, str(theme_overall)])
            l_stats.append([sent_id, sent, str(theme_name)])

        if theme.startswith('5.'):
            theme_overall = 5
            theme_name = "Reflections"
            l.append([sent_id, sent, str(theme_overall)])
            l_stats.append([sent_id, sent, str(theme_name)])

    for item_stats in l_stats:
        if item_stats not in l_nodup_stats:
            l_nodup_stats.append(item_stats)

    for item in l:

        if item not in l_nodup:
            l_nodup.append(item)

    labels = ['id', 'sentence', 'theme']
    df_first = pd.DataFrame.from_records(l_nodup_stats, columns=labels)

    d = {}
    for elem in l_nodup:
        try:
            d[elem[0]].append(elem[2])
        except KeyError:
            d[elem[0]] = [elem[2]]

    d_new = {}
    for key, val in d.items():
        val_new = set(val)
        val_new = list(val_new)
        d_new[key] = val_new

    d_1 = {}
    for elem in l_nodup:
        # try:
        # d_1[elem[0]].append(elem[1])
        # except KeyError:
        d_1[elem[0]] = elem[1]

    df_1 = pd.DataFrame.from_dict(d_1.items())
    df = pd.DataFrame.from_dict(d_new.items())
    df_merged = pd.merge(df_1, df, on=0)

    return df_merged, df_first

def clean_text(sentence):
    #clean each sentence from the dataframe and and turns it into list of tokens
    sentence = str(sentence)
    tokens = []

   #stopwrds = stop_words

    sentence = re.sub(r'<.*?>', '', sentence)
    sentence = re.sub(r"\\", "", sentence)
    sentence = re.sub(r"\'", "", sentence)
    sentence = re.sub(r"\"", "", sentence)
    sentence = sentence.strip().lower()
    wordnet_lemmatizer = WordNetLemmatizer()

    for word in sentence.split():
        if word not in stop_words:
            #word = re.escape(word)
            #word = "".join(l for l in word if l not in string.punctuation)
            #word = "".join(l for l in word if not l.isdigit())
            #printable = set(string.printable)
            #word = filter(lambda x: x in printable, word)
            #word = re.escape(word)
            word = wordnet_lemmatizer.lemmatize(word)

            #if word.strip() not in stop_words:
            tokens.append(word)

    return tokens

def splitData(X,y):
    X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state=5)
    return X_train,X_test,y_train,y_test

def createFeatureVectors_count(X_df, X_train, X_test):

    vectorizer = CountVectorizer(max_features=1000,ngram_range=(1,2))
    vectorizer.fit(X_df[1])
    feat_names = vectorizer.get_feature_names()
    vector_train = vectorizer.transform(X_train[1])
    vector_test = vectorizer.transform(X_test[1])

    return vector_train, vector_test, feat_names

def TFIDFVectors(X_df,X_train, X_test):
    vectorizer = TfidfVectorizer(sublinear_tf=True, use_idf=True,max_features=1000,stop_words = "english",ngram_range=(1, 2))

    X = vectorizer.fit_transform(X_df[1])
    idf = vectorizer.idf_

    vector_train = vectorizer.transform(X_train[1])
    vector_test = vectorizer.transform(X_test[1])
    feat_names = vectorizer.get_feature_names()


    return vector_train, vector_test, feat_names

def encodeVectors(y_df, y_train, y_test):
    y_df_1 = y_df.drop(y_df.columns[0], axis=1)
    y_train_1 = y_train.drop(y_train.columns[0], axis=1)
    y_test_1 = y_test.drop(y_test.columns[0], axis=1)
    mlm = preprocessing.MultiLabelBinarizer()

    y_all_labels = mlm.fit_transform(y_df_1[1])
    y_train_labels = mlm.transform(y_train_1[1])
    y_test_labels = mlm.transform(y_test_1[1])

    encoded_y_df = list(zip(y_df[0], y_all_labels))
    encoded_y_train = list(zip(y_train[0], y_train_labels))
    encoded_y_test = list(zip(y_test[0], y_test_labels))

    encoded_all_df = pd.DataFrame(encoded_y_df)
    encoded_y_train_df = pd.DataFrame(encoded_y_train)
    encoded_y_test_df = pd.DataFrame(encoded_y_test)

    return encoded_all_df, encoded_y_train_df, encoded_y_test_df, y_all_labels, y_train_labels, y_test_labels

def createModel_count(vector_train, y_train_labels):

    model = OneVsRestClassifier(ComplementNB(alpha=1.0, fit_prior=False, class_prior=None))
    #model = OneVsRestClassifier(MultinomialNB(alpha=1.0, fit_prior=False, class_prior=None))

    model.fit(vector_train,y_train_labels)
    scoring = ['precision_macro', 'recall_macro','precision_micro','recall_micro']

    macro_p = cross_validate(model, vector_train, y_train_labels, scoring='precision_macro')
    macro_r = cross_validate(model, vector_train, y_train_labels, scoring='recall_macro')
    micro_p = cross_validate(model, vector_train, y_train_labels, scoring='precision_micro')
    micro_r = cross_validate(model, vector_train, y_train_labels, scoring='recall_micro')
    print(macro_p)
    print(macro_r)
    print(micro_p)
    print(micro_r)


    filename = 'baseline_count2.sav'
    pickle.dump(model, open(filename, 'wb'))

def createModel_svc(vector_train, y_train_labels):

    model = OneVsRestClassifier(SVC(gamma='scale', kernel= 'linear',decision_function_shape='ovo',probability=True))
    #model = OneVsRestClassifier(MultinomialNB(alpha=1.0, fit_prior=False, class_prior=None))

    model.fit(vector_train,y_train_labels)
    filename = 'baseline_count1.sav'
    pickle.dump(model, open(filename, 'wb'))

def predictModel_count(encoded_y_train_df,vector_test):
    #model = pickle.load(open('model_stratified.sav', 'rb'))
    #model = pickle.load(open('model_cNB_tfidf_2.sav', 'rb'))
    model = pickle.load(open('baseline_count2.sav', 'rb'))

    y_predict = model.predict(vector_test)
    zipped = zip(encoded_y_train_df[0], encoded_y_train_df[1], y_predict)
    return y_predict, zipped

def evaluate_count(y_test_labels, y_predict, X_test):
    print(y_test_labels)
    print(y_predict)
    classif_rep = classification_report(y_test_labels, y_predict, target_names=['1','2','3','4','5'])
    print(classif_rep)

def show_most_informative_features_count(feat_names):
    clf = pickle.load(open('baseline_count2.sav', 'rb'))
    class_labels = clf.classes_
    feature_names = feat_names
    for i, class_label in enumerate(class_labels):
        top10 = np.argsort(clf.coef_[i])[-10:]
        print("%s: %s" % (class_label, ",".join(feature_names[j] for j in top10)))


def getVectorsX():

    x_values = {}
    #sql = "SELECT sent_id,sent from cleaned_lemmatised;"
    sql = "SELECT sent_id,sentence from second_classifier_train;"
    #print(sql)
    sql = "SELECT sent_id,sentence from intitial_classifier_train;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        tokens = []
        sent_id = str(row[0])
        sent = str(row[1])
        for word in sent.split():


            tokens.append(word)

        sent_new = ' '.join(token for token in tokens)

        x_values[sent_id] = sent_new
        #x_values[sent_id] = sent
    df_x = pd.DataFrame.from_dict(x_values.items())
    return df_x


def getVectorsY():

    y_values = {}
    #sql = "SELECT sent_id,label from cleaned_lemmatised;"
    sql = "SELECT sent_id,label from second_classifier_train;"
    #sql = "SELECT sent_id,label from intitial_classifier_train;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        label = str(row[1].replace("[", "").replace("]", "").replace("'", "")).split(", ")
        labels_int = []

        for l in label:
            l = l.strip()
            l = int(l)
            labels_int.append(l)
        y_values[sent_id] = labels_int

    df_y = pd.DataFrame.from_dict(y_values.items())

    return df_y

def getTestSent():
    x_values = {}
    sql = "SELECT sent_id,sent from unseens_sent where label not like '[10%';"
    #sql = "SELECT passage_id,passage from unseens_passages;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        tokens = []
        sent_id = str(row[0])
        sent = str(row[1])
        x_values[sent_id] = sent

    X_test = pd.DataFrame.from_dict(x_values.items())
    return X_test

def getTestLabels():
    y_values = {}
    sql = "SELECT sent_id,label from unseens_sent  where label not like '[10%';"
    #sql = "SELECT passage_id,label from unseens_passages;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        label = str(row[1].replace("[", "").replace("]", "").replace("'", "")).split(", ")
        labels_int = []

        for l in label:
            l = l.strip()
            l = str(l)
            labels_int.append(l)
        y_values[sent_id] = labels_int

    y_test = pd.DataFrame.from_dict(y_values.items())

    return y_test

def decodeVectors(y_df,y_predict,y_test_labels):
    mlm = preprocessing.MultiLabelBinarizer()
    y_df_1 = y_df.drop(y_df.columns[0], axis=1)
    y_all_labels = mlm.fit_transform(y_df_1[1])
    predicted_classes = mlm.inverse_transform(y_predict)
    test_classes = mlm.inverse_transform(y_test_labels)
    return test_classes,predicted_classes

def main():

    df, df_first = readDB()
    #warnings.filterwarnings('ignore')

    X_dict = {}
    y_dict = {}
    X = []

    for i in range(df.shape[0]):

        tokens = clean_text(df.iloc[i]['1_x'])
        if len(tokens) >= 2:
            sent = ' '.join(token for token in tokens)

            X_dict[df.iloc[i][0]] = sent
            y_dict[df.iloc[i][0]] = df.iloc[i]['1_y']
            X.append(sent)

    X_df = pd.DataFrame.from_dict(X_dict.items())
    y_df = pd.DataFrame.from_dict(y_dict.items())

    #X_df = getVectorsX()
    #y_df = getVectorsY()
    X_train, X_test, y_train, y_test = splitData(X_df, y_df)




    #X_test = getTestSent()
    #y_test = getTestLabels()



    vector_train, vector_test, feat_names = createFeatureVectors_count(X_df, X_train, X_test)

    #vector_train, vector_test, feat_names = TFIDFVectors(X_df, X_train, X_test)
    encoded_all_df, encoded_y_train_df, encoded_y_test_df, y_all_labels, y_train_labels, y_test_labels = encodeVectors(y_df, y_train, y_test)
    createModel_count(vector_train, y_train_labels)
    #createModel_svc(vector_train, y_train_labels)
    y_predict, zipped_1 = predictModel_count(encoded_y_train_df, vector_test)
    test_classes, predicted_classes = decodeVectors(y_df, y_predict, y_test_labels)
    zipped = list(zip(X_test[0], predicted_classes))


    evaluate_count(y_test_labels, y_predict, X_test)
    show_most_informative_features_count(feat_names)

main()