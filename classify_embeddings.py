import sqlite3
import pandas as pd
from sklearn import svm
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.multiclass import OneVsRestClassifier
import pickle
from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from decimal import Decimal
from sklearn.ensemble import AdaBoostClassifier
import numpy as np
import scipy
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import OPTICS, cluster_optics_dbscan
#from wordcloud import WordCloud, STOPWORDS
from scipy.sparse import csr_matrix
from numpy import reshape
from gensim.matutils import softcossim
import itertools
from collections import Counter
#from pyod.models.abod import ABOD
#from pyod.models.knn import KNN
from mpl_toolkits.mplot3d import Axes3D
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import ComplementNB
from sklearn.tree import DecisionTreeClassifier
from collections import Counter
from sklearn.ensemble import VotingClassifier
from sklearn.manifold import TSNE
from sklearn.ensemble import BaggingClassifier
#from mlxtend.classifier import StackingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.calibration import CalibratedClassifierCV





conn = sqlite3.connect('')
cur = conn.cursor()

def getWordFeautres():
    word_embeddings = {}
    sql = "SELECT term, term_count, vector FROM entire_set_dict_new where exist = 'yes';"
    cur.execute(sql)
    result = cur.fetchall()
    count = 0
    for row in result:
        term = str(row[0])
        term_count = int(row[1])
        vector = str((row[2]))
        vector_l = vector.split(" ")
        scores = []
        for score in vector_l:
            score = score.replace("\n","").strip().replace(',', '.').replace("[","").replace("]","")
            if len(score) != 0:
                score = float(score)
                scores.append(score)

        word_embeddings[term] = scores
        count = count+1

    return count,word_embeddings

def tokeniseSent():
    tokenised_sent = []
    sql = "SELECT sent_id, sent from cleaned_lemmatised;"

    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        sent = str(row[1])
        tokens = []
        for w in sent.split():
            tokens.append(w)

        twograms = zip(*[tokens[i:] for i in range(2)])
        list_twograms = list(twograms)
        new_twos = []
        for el in list_twograms:
            el = str(el).replace("('","").replace("')","").replace("', '"," ")
            new_twos.append(el)
        all_tokens = tokens+new_twos
        tokenised_sent.append([sent_id,all_tokens])

    return tokenised_sent

def filterSent(word_embeddings,tokenised_sent):
    vocab = word_embeddings.keys()
    filtered_sent = []
    word2sent_vector = []
    for i in range(0,len(tokenised_sent)):
        if tokenised_sent[i] in vocab:
            filtered_sent.append(tokenised_sent[i])
            word2sent_vector.append(word_embeddings[tokenised_sent[i]])
    return filtered_sent,word2sent_vector

def insertFiltertedSent(sent_id,filtered_sent):
    sql = 'INSERT INTO filtered_sent(sent_id,sent_vector) VALUES("'+str(sent_id)+'", "'+str(filtered_sent)+'");'

    cur.execute(sql)
    conn.commit()

def insertSentToVec(sent_id,sent2vec):
    sql = "INSERT INTO sentTwoVec(sent_id,vec) VALUES('" + str(sent_id) + "','" + str(sent2vec) + "');"

    cur.execute(sql)
    conn.commit()

def getSentIDsWithZeroVec():
    ids = []
    sql = "select sent_id,tfidf from sentvectors_embeddings_fastext where tfidf like '[0.%';"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        sent_id = row[0]
        tfidf = row[1]
        ids.append(sent_id)

    ids_str = str(ids).replace("[","(").replace("]",")")

    return ids_str


def getTestingIDS():
    ids = []
    sql = "SELECT sent_id FROM newVotingClassifier"
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        sid = row[0]
        ids.append(sid)

    ids_suitable = str(ids).replace("[","(").replace("]",")")

    return ids_suitable

def getVectorsX():
    ids_suitable = getTestingIDS()
    x_values = {}

    sql = "SELECT sent_id, avg_spvec_usif FROM sentvectors_embeddings_fastext where not sent_id in " + ids_suitable + " and avg_spvec_usif is not null;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        #sent = row[1]

        vec = str(row[1]).replace("[","").replace("]","")
        vec = vec.split()
        float_vec = []
        for v in vec:
            v = v.strip()
            v = float(v)



            float_vec.append(v)

        x_values[sent_id] = float_vec


    df_X = pd.DataFrame.from_dict(x_values.items())

    return df_X

def getVectorsY():
    ids_suitable = getTestingIDS()
    y_values = {}

    sql = "SELECT sent_id, label FROM sentvectors_embeddings_fastext where not sent_id in " + ids_suitable + " and avg_spvec_usif is not null;"

    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])

        label = row[1].replace("[", "").replace("]", "").replace("'", "")


        labels_int = []

        if "," in label:
            for l in label.split(","):
                l = l.strip()
                l = int(l)
                labels_int.append(l)
        else:
            label = int(label.strip())
            labels_int.append(label)

        y_values[sent_id] = labels_int
    df_y = pd.DataFrame.from_dict(y_values.items())


    return df_y

def getVectorsX_test():
    ids_suitable = getTestingIDS()
    x_values = {}

    sql = "SELECT sent_id, avg_spvec_usif FROM sentvectors_embeddings_fastext where sent_id in " + ids_suitable + " and avg_spvec_usif is not null;"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        #sent = row[1]

        vec = str(row[1]).replace("[","").replace("]","")
        #vec = vec.split(",")
        vec = vec.split()
        float_vec = []
        for v in vec:
            v = v.strip()
            v = float(v)
            #v = v+1000.0


            float_vec.append(v)

        x_values[sent_id] = float_vec

        #x_values[sent_id] = sent
    df_X = pd.DataFrame.from_dict(x_values.items())

    return df_X

def getVectorsY_test():
    ids_suitable = getTestingIDS()
    y_values = {}

    sql = "SELECT sent_id,label from sentvectors_embeddings_fastext where sent_id in " + ids_suitable + " and avg_spvec_usif is not null;"

    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])

        label = row[1].replace("[", "").replace("]", "").replace("'", "")


        labels_int = []

        if "," in label:
            for l in label.split(","):
                l = l.strip()
                l = int(l)
                labels_int.append(l)
        else:
            label = int(label.strip())
            labels_int.append(label)

        y_values[sent_id] = labels_int
    df_y = pd.DataFrame.from_dict(y_values.items())


    return df_y

def getVectorsXTrain():
    x_values = {}
    sql = "SELECT sent_id,vec from sentTwoVec_unseens_labels where vec != '[]';"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        if sent_id != 'doc_2.385.1' and sent_id != 'doc_2.385.2' and sent_id != 'doc_2.385.3' and sent_id != 'doc_8.12.1' and sent_id != 'doc_8.12.2' and sent_id != 'doc_8.12.3' and sent_id != 'doc_1.346.1' and sent_id != 'doc_1.346.2' and sent_id != 'doc_1.346.3' and sent_id != 'doc_18.20.1' and sent_id != 'doc_18.20.2' and sent_id != 'doc_18.20.3' and sent_id != 'doc_3.13.1' and sent_id != 'doc_3.13.2' and sent_id != 'doc_3.13.3':
            vec = str(row[1]).replace("[","").replace("]","")
            vec = vec.split(", ")
            #vec = vec.split()
            float_vec = []
            for v in vec:


                v = float(v)




                float_vec.append(v)
            x_values[sent_id] = float_vec

    df_X = pd.DataFrame.from_dict(x_values.items())
    return df_X


def getVectorsYTrain():
    y_values = {}
    sql = "SELECT sent_id,label from sentTwoVec_unseens_labels where vec != '[]';"
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        sent_id = str(row[0])
        if sent_id != 'doc_2.385.1' and sent_id != 'doc_2.385.2' and sent_id != 'doc_2.385.3' and sent_id != 'doc_8.12.1' and sent_id != 'doc_8.12.2' and sent_id != 'doc_8.12.3' and sent_id != 'doc_1.346.1' and sent_id != 'doc_1.346.2' and sent_id != 'doc_1.346.3' and sent_id != 'doc_18.20.1' and sent_id != 'doc_18.20.2' and sent_id != 'doc_18.20.3' and sent_id != 'doc_3.13.1' and sent_id != 'doc_3.13.2' and sent_id != 'doc_3.13.3':
            label = str(row[1].replace("[", "").replace("]", "").replace("'", "")).split(", ")
            labels_int = []

            for l in label:
                l = l.strip()
                l = int(l)
                labels_int.append(l)
            y_values[sent_id] = labels_int

    df_y = pd.DataFrame.from_dict(y_values.items())

    return df_y

def encodeVectors(y_df, y_train, y_test):
    y_df_1 = y_df.drop(y_df.columns[0], axis=1)
    y_train_1 = y_train.drop(y_train.columns[0], axis=1)
    y_test_1 = y_test.drop(y_test.columns[0], axis=1)
    mlm = preprocessing.MultiLabelBinarizer()

    y_all_labels = mlm.fit_transform(y_df_1[1])
    y_train_labels = mlm.transform(y_train_1[1])
    y_test_labels = mlm.transform(y_test_1[1])

    encoded_y_df = list(zip(y_df[0], y_all_labels))
    encoded_y_train = list(zip(y_train[0], y_train_labels))
    encoded_y_test = list(zip(y_test[0], y_test_labels))

    encoded_all_df = pd.DataFrame(encoded_y_df)
    encoded_y_train_df = pd.DataFrame(encoded_y_train)
    encoded_y_test_df = pd.DataFrame(encoded_y_test)

    return encoded_all_df, encoded_y_train_df, encoded_y_test_df, y_all_labels, y_train_labels, y_test_labels

def splitData(X,y):
    X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=0.2,random_state=5)

    return X_train,X_test,y_train,y_test

def createModel(vector_train, y_train_labels):


    model = OneVsRestClassifier(GaussianNB())

    model.fit(vector_train,y_train_labels)
    filename = 'sentTwoVec1.sav'
    pickle.dump(model, open(filename, 'wb'))



def predictModel(vector_test):
    model = pickle.load(open('sentTwoVec1.sav', 'rb'))
    y_predict = model.predict(vector_test)
    print(y_predict)
    return y_predict

def evaluate_count(y_test_labels, y_predict, X_test):
    classif_rep = classification_report(y_test_labels, y_predict, target_names=['1','2','3','4','5'])
    print(classif_rep)

def insertData():
    count, word_embeddings = getWordFeautres()
    tokenised_sent = tokeniseSent()
    for i in range(0, len(tokenised_sent)):
        filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
        sent2vec = np.mean([word_embeddings[word] for word in filtered_sent], axis=0)
        insertFiltertedSent(tokenised_sent[i][0],filtered_sent)
        insertSentToVec(tokenised_sent[i][0], sent2vec)

def decodeVectors(y_df,y_predict,y_test_labels):
    mlm = preprocessing.MultiLabelBinarizer()
    y_df_1 = y_df.drop(y_df.columns[0], axis=1)
    y_all_labels = mlm.fit_transform(y_df_1[1])
    predicted_classes = mlm.inverse_transform(y_predict)
    test_classes = mlm.inverse_transform(y_test_labels)
    return test_classes,predicted_classes

def classification():

    y_df = getVectorsY()

    X_train = getVectorsX()
    y_train = getVectorsY()

    X_test = getVectorsX_test()
    y_test = getVectorsY_test()




    encoded_all_df, encoded_y_train_df, encoded_y_test_df, y_all_labels, y_train_labels, y_test_labels = encodeVectors(y_df, y_train, y_test)
    vector_train = list(X_train[1])
    createModel(vector_train, y_train_labels)
    y_predict = predictModel(list(X_test[1]))
    test_classes, predicted_classes = decodeVectors(y_df,y_predict,y_test_labels)
    zipped = list(zip(X_test[0],predicted_classes))

    for i in range(0, len(zipped)):
        sent_id = str(zipped[i][0])
        labels = str(zipped[i][1]).replace("(","[").replace(",)","]").replace(")","]")


    evaluate_count(y_test_labels, y_predict, X_test)

def insertDataVoting():
    ids = getTestingIDS()
    X_df = getVectorsX()
    y_df = getVectorsY()
    X_train, X_test, y_train, y_test = splitData(X_df, y_df)
    df_merged = pd.merge(X_test, y_test, on=0)
    for i in range(df_merged.shape[0]):
        sent_id = str(df_merged.iloc[i][0])
        sent = str(df_merged.iloc[i]['1_x'])
        actual_label = str(df_merged.iloc[i]['1_y'])


    for j in range(y_test.shape[0]):
        if y_test.iloc[j][0] not in ids:
            print(y_test.iloc[j][0])

    print("------------------")


def stats():
    X_df = getVectorsX()
    y_df = getVectorsY()
    X_train, X_test, y_train, y_test = splitData(X_df, y_df)
    #print(y_train)
    listoflabels = []
    for i in range(y_test.shape[0]):
        sentid = y_test.iloc[i][0]
        labels = str(y_train.iloc[i][1]).replace("[","").replace("]","").split(",")
        for l in labels:
            l = l.strip()
            listoflabels.append([sentid,l])

    count_labels = pd.DataFrame(listoflabels)
    counts = Counter(count_labels[1])
    numbers = []
    bars = []

    for i in counts.items():
        if i[0] == '1':
            numbers.append('Contact with Agencies')
            bars.append(i[1])
        if i[0] == '2':
            numbers.append('Indicative behaviour')
            bars.append(i[1])
        if i[0] == '3':
            numbers.append('Indicative circumstances')
            bars.append(i[1])
        if i[0] == '4':
            numbers.append('Mental Health Issues')
            bars.append(i[1])
        if i[0] == '5':
            numbers.append('Reflections')
            bars.append(i[1])

    #print("numbers: ", numbers)
    #print("bars: ", bars)
    print(counts)

def FindCosineSimSImple():
    count, word_embeddings = getWordFeautres()
    tokenised_sent = tokeniseSent()

    count = 0
    sample = []
    for i in range(0, len(tokenised_sent)):
        filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
        sent2vec = np.mean([word_embeddings[word] for word in filtered_sent], axis=0)
        if count < 6:
            sample.append([tokenised_sent[i][0], sent2vec, tokenised_sent[i][1]])
        count = count + 1

    all_cosines = []
    columns_matrix = []
    for i in range(0,len(sample)):
        vector_i = sample[i][1]
        sent_id_i = sample[i][0]
        tokens_i = sample[i][2]
        cosine_per_sent = []
        columns_matrix.append(sent_id_i)
        for j in range(0,len(sample)):
            vector_j = sample[j][1]
            send_id_j = sample[j][0]
            tokens_j = sample[j][2]

            cosine = scipy.spatial.distance.cosine(vector_i, vector_j)
            cosine_precentage = round((1 - cosine) * 100, 2)


            cosine_per_sent.append(cosine_precentage)

        all_cosines.append(cosine_per_sent)
    m = reshape(all_cosines, (6, 6))

    df = pd.DataFrame(m,columns=columns_matrix,index=columns_matrix)
    print(df)


def calculatedWeightedSentVec():
    a = 1e-3
    a = float(a)

    count, word_embeddings = getWordFeautres()
    tokenised_sent = tokeniseSent()
    all_docs = []

    for i in range(0, len(tokenised_sent)):
        filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
        all_docs.append(filtered_sent)

    word_counts = Counter(itertools.chain(*all_docs))
    sentence_set = []
    for i in range(0,len(all_docs)):
        sentence = tokenised_sent[i][1]
        vs = np.zeros(300)
        sentence_length = len(sentence)
        for word in sentence:

            a_value = a / (a + word_counts[word])  # smooth inverse frequency, SIF

            try:
                vs = np.add(vs, np.multiply(a_value, word_embeddings[word]))  # vs += sif * word_vector
            except Exception as e:
                pass
        vs = np.divide(vs, sentence_length)  # weighted average
        sentence_set.append(vs)
    return sentence_set,tokenised_sent

def clusterOnWeightedVal(sentence_set,tokenised_sent):
    X = sentence_set
    tokenised_sent = tokenised_sent



    kmeans = KMeans(n_clusters=4)
    kmeans.fit(X)
    assigned_clusters = kmeans.labels_
    # print(len(assigned_clusters))

    for j in range(0, len(tokenised_sent)):
        print(str(assigned_clusters[j]) + ":" + str(tokenised_sent[j][0] + ":" + str(tokenised_sent[j][1])))

    pca = PCA(n_components=2)
    result = pca.fit_transform(X)
    plt.scatter(result[:, 0], result[:, 1], c=assigned_clusters)
    plt.savefig('cluster_weighted_4_graph.png')


def calculatedSimpleSentVec():
    count, word_embeddings = getWordFeautres()
    tokenised_sent = tokeniseSent()

    count = 0
    sample = []

    X = []
    sentences = []
    count = 0
    for i in range(0, len(tokenised_sent)):

        filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
        sent2vec = np.mean([word_embeddings[word] for word in filtered_sent], axis=0)

        sentences.append([tokenised_sent[i][0], tokenised_sent[i][1]])
        if count < 6:
            X.append(sent2vec)
        count = count + 1

    pca = PCA(n_components=2)
    result = pca.fit_transform(X)

    print(len(result))
    # plt.figure(figsize=(8, 8), facecolor=None)
    labels = []
    for i in range(0, len(result)):
        #print(result[i][0],result[i][1],sentences[i][0])
        labels.append(sentences[i][0])
        plt.text(result[i][0], result[i][1], sentences[i][0])
        #plt.scatter(result[i][0], result[i][1])
    plt.scatter(result[:, 0], result[:, 1])
    plt.show()
    #plt.savefig('experiment_presentation.png')

def clusterSent():
    '''
    count, word_embeddings = getWordFeautres()

    tokenised_sent = tokeniseSent()
    X = []
    sentences = []
    count = 0
    for i in range(0, len(tokenised_sent)):
        if tokenised_sent[i][0] not in ['12.13','14.63','8.255','7.134','7.135','8.64']:


            filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
            sent2vec = np.mean([word_embeddings[word] for word in filtered_sent], axis=0)
            sentences.append([tokenised_sent[i][0],tokenised_sent[i][1]])


            X.append(sent2vec)
            count = count + 1
    '''
    X = getVectorsX()
    #kmeans = KMeans(n_clusters=4)
    kmeans = OPTICS().fit(list(X[1]))
    kmeans.fit(list(X[1]))
    assigned_clusters = kmeans.labels_
    print( assigned_clusters)
    #print(len(assigned_clusters))

    #for j in range(0, len(sentences)):
    #    print(str(assigned_clusters[j]) + ":" + str(sentences[j][0] + ":" + str(sentences[j][1])))

        #sql = "INSERT INTO clusters(sent_id, cluster_3) VALUES('"+str(sentences[j][0])+"', '"+str(assigned_clusters[j])+"');"
        #sql = "UPDATE clusters set cluster_4 = '"+str(assigned_clusters[j])+"' where sent_id = '"+str(sentences[j][0])+"';"
        #cur.execute(sql)
        #conn.commit()

    result = TSNE(n_components=2).fit_transform(list(X[1]))
    #pca = PCA(n_components=2)
    #result = pca.fit_transform(X)
    print(len(result))
    #plt.figure(figsize=(200,240), facecolor='w')
    #plt.scatter(result[:, 0], result[:, 1], c=assigned_clusters)
    #plt.show()
    #ax = plt.gca(projection='3d')

    #for i in range(0,len(result)):
    #    print(result[i][0],result[i][1])
        #labels.append(sentences[i][0])
        #plt.text(result[i][0], result[i][1],sentences[i][0])
    plt.scatter(result[:,0], result[:,1],c=kmeans.labels_.astype(float))
    #plt.scatter(result[:,0], result[:,1],c=assigned_clusters)
    plt.show()



    #plt.scatter(result[:, 0], result[:, 1],c=kmeans.labels_,label=sentences[i][0])
    #plt.savefig('cluster_5_graph.png')
    #plt.show()

    #pca = PCA(n_components=2)
    #result = pca.fit_transform(X)


def dbscanCluster():
    count, word_embeddings = getWordFeautres()

    tokenised_sent = tokeniseSent()
    X = []
    sentences = []
    count = 0
    for i in range(0, len(tokenised_sent)):
        filtered_sent, word2sent_vector = filterSent(word_embeddings, tokenised_sent[i][1])
        sent2vec = np.mean([word_embeddings[word] for word in filtered_sent], axis=0)
        sentences.append([tokenised_sent[i][0], tokenised_sent[i][1]])
        X.append(sent2vec)

    #dbsc = DBSCAN(eps=.00001, min_samples=100).fit(X)
    clust = OPTICS().fit(X)
    labels = clust.labels_
    print(labels)

    pca = PCA(n_components=2)
    result = pca.fit_transform(X)

    plt.scatter(result[:, 0], result[:, 1], c=labels)
    plt.show()


def wordCloudsClusters():
    comment_words = ' '
    word_counts = {}
    sql = "SELECT s.sent_vector from filtered_sent s, clusters c where c.sent_id = s.sent_id and c.cluster_4 = 3;"
    cur.execute(sql)
    result = cur.fetchall()

    for row in result:
        val = row[0].replace("[","").replace("]","").replace("'","")
        for el in val.split(","):
            el = el.strip()
            comment_words = comment_words + el + ' '
            if el in word_counts:
                word_counts[el] = word_counts[el] + 1
            else:
                word_counts[el] = 1

    sorted_tokens_theme = []
    count = 0
    a1_sorted_keys = sorted(word_counts, key=word_counts.get, reverse=True)
    for r in a1_sorted_keys:
        count = count + 1
        # if count >= 30:
        sorted_tokens_theme.append([r, word_counts[r]])



    stopwords = set(STOPWORDS)
    wordcloud = WordCloud(width=800, height=800,background_color='white',stopwords=stopwords,min_font_size=10).generate(comment_words)

    # plot the WordCloud image
    plt.figure(figsize=(8, 8), facecolor=None)
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.tight_layout(pad=0)
    #plt.savefig("cluster_new_4_3.png")

    #plt.show()

    #print(sorted_tokens_theme)
    for el in sorted_tokens_theme:
        print(el[0],el[1])
        print("------------------------------")

def ensemblingClassification():
    X_df = getVectorsX()
    y_df = getVectorsY()

    X_train, X_test, y_train, y_test = splitData(X_df, y_df)
    encoded_all_df, encoded_y_train_df, encoded_y_test_df, y_all_labels, y_train_labels, y_test_labels = encodeVectors(y_df, y_train, y_test)
    vector_train = list(X_train[1])
    vector_test = list(X_test[1])
    model1 = OneVsRestClassifier(GaussianNB())
    #model1_score = model1.fit(vector_train, y_train_labels).score(vector_test,y_test_labels)
    #y_predict = model1.predict(list(X_test[1]))
    #evaluate_count(y_test_labels_wd, y_predict_wd, X_test_wd)
    #print(y_predict)
    #print("-----------------------")
    model2 = OneVsRestClassifier(SVC(gamma='scale', kernel= 'poly',decision_function_shape='ovo',probability=True))
    #model2_score = model2.fit(vector_train, y_train_labels).score(vector_test,y_test_labels)
    #y_predict_wd = model2.predict(list(X_test[1]))
    #weight_list = [[1,0],[2,1],[3,1],[4,1],[5,1],[6,1],[2,3],[3,2]]
    model3 = OneVsRestClassifier(KNeighborsClassifier(n_neighbors=5))

    for w1 in range(1, 4):
        for w2 in range(1, 4):
            for w3 in range(1, 4):
                est_Ensemble = OneVsRestClassifier(VotingClassifier(estimators=[('GNB', model1), ('SVM', model2),('KN', model3)],voting='soft',weights=[w1,w2,w3]))
                score_Ensemble = est_Ensemble.fit(vector_train, y_train_labels).predict(list(X_test[1]))
                classif_rep = classification_report(y_test_labels, score_Ensemble, target_names=['1', '2', '3', '4', '5'])
                print("THE weights for the classifier are: ",w1,w2,w3)
                print(classif_rep)
                print("--------------------------------------------------")
                with open('ensemble_inferSent.txt','a') as f:
                    f.write(str(["THE weights for the classifier are: ",w1,w2,w3]+ '\n'))
                    f.write(str([classif_rep]+ '\n'))
                    f.write(["---------------------------------------------------"]+ '\n')



def main():
    #FindCosineSimSImple()
    #calculatedSimpleSentVec()
    #clusterSent()
    #wordCloudsClusters()
    #sentence_set, tokenised_sent = calculatedWeightedSentVec()
    #clusterOnWeightedVal(sentence_set, tokenised_sent)

    #wordCloudsClusters()
    #dbscanCluster()

    #getSentIDsWithZeroVec()
    #count, word_embeddings = getWordFeautres()
    #tokenised_sent = tokeniseSent()
    #filtered_sent,word2sent_vector = filterSent(word_embeddings, tokenised_sent)
    #print(filtered_sent)
    classification()
    #stats()
    #ensemblingClassification()
    #stackingClassifiers()
    #insertDataVoting()






main()